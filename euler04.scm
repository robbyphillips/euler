; Project Euler Problem 4
; Find the largest palindrome made from the product of two 3-digit numbers.

(define (palindrome? n)
  (cond ((integer? n) (palindrome? (string n))) 
	((not (string? n)) (error "palindrome? takes 1 string as arg"))
	((= 0 (string-length n)) (error "string length = 0"))
	(else 
	 (let ((ls (string->list n)))
	   (if (symmetric? ls) #t #f)))))


(define (symmetric? ls #!optional i)
  (if (eq? i #!default) (symmetric? ls 0)
      (if (eq? (list-ref ls i) (list-ref ls (- (length ls) (+ i 1))))
	  (if (< i (/ (length ls) 2))
	      (eq? #t (symmetric? ls (+ i 1)))
	      #t)
	  #f)))

(define (largest-possible-palindrome x)
;;Where x is the number of digits each number multiplied
  (define (helper x y upper lower output)
    (cond ((<= x lower) (max-list output))
	  ((<= y lower) (helper (- x 1) upper upper lower output))
	  ((palindrome? (* x y))
	   (helper x (- y 1) upper lower (cons (* x y) output)))
	  (else (helper x (- y 1) upper lower output))))
  (let ((u (- (expt 10 x) 1))
	(l (expt 10 (- x 1)))
	(p-list '()))
    (helper u u u l p-list)))



;; Returns the max value of a list of numbers
;; INPUTS: Exactly one list of numberical values
;; OUTPUT: numerical value
(define (max-list l)
  (cond ((null? (cdr l)) (car l))
	((> (car l) (cadr l)) (max-list (cons (car l) (cddr l))))
	(else (max-list (cdr l)))))