;;;; Project Euler Problem 5
;;;   Find the smallest number that is evenly divisible
;;;   by all the numbers 1 through 20.

;; euler -- Finds the smallest integer evenly divisible by all
;;          integers between the given lower and upper bounds
;; INPUTS:  Two arguments a and b
;;          a -- lower bound -- integer
;;          b -- upper bound -- integer
;; OUTPUT:  integer
(define (euler lower upper)
  (define (helper x y i) ; i -- iteration -- integer
    (cond ((> x y) i)
	  ((= (remainder i y) 0)
	   (display i)
	   (display " / ")
	   (display y)
	   (newline)
	   (helper x (- y 1) i))
	  (else (helper lower upper (+ i 1)))))
  (helper lower upper 200042840))


