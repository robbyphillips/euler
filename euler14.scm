;;;; Project Euler Problem 14
;;;  Using the following iterative sequence, what is
;;;  the longest chain that can be produced starting
;;;  with a number under 1,000,000?
;;;  n -> n/2  (when n is even)
;;;  n -> 3n+1 (when n is odd)

(define (nextn n)
  (cond ((<= n 1) '())
	((even? n) (/ n 2))
	(else (+ 1 (* 3 n)))))

(define (chain-length n chain)
  (cond ((null? n) (length chain))
	(else (chain-length (nextn n) (cons n chain)))))

(define (chain-len n i)
  (cond ((<= n 1) i)
	((odd? n) (chain-len (+ (* 3 n) 1) (1+ i)))
	(else
	 (chain-len (/ n 2) (1+ i)))))

(define (euler bound i besti bestval)
  (let ((val (chain-len i 0)))
    (cond ((> i bound) (display bestval) besti)
	  ((> val bestval)
	   (display i) (display ":  ")
	   (display val) (newline)
	   (euler bound (+ 1 i) i val))
	  (else
	   (euler bound (1+ i) besti bestval)))))

;(euler 999999 0 0 0)
;; Answer: 837799