;;;; Project Euler Problem 17
;;;  How many letters would be used to write out all the numbers
;;;  from 1 to 1000 in words.  Don't count spaces or hyphens.

(define (intens? x)
  (and (< x 100) (> x 9)))

(define (place x)
  (

(define numwords
  ((0 . "zero") (1 . "one") (2 . "two") (3 . "three") (4 . "four")
   (5 . "five") (6 . "six") (7 . "seven") (8 . "eight") (9 . "nine")))

(define tens-numwords
  ((10 . "ten") (11 . "eleven") (12 . "twelve") (13 . "thirteen") (14 . "fourteen")
   (15 . "fifteen") (16 . "sixteen") (17 . "seventeen") (18 . "eighteen") (19 . "nineteen")
   (20 . "twenty") (30 . "thirty") (40 . "forty") (50 . "fifty") (60 . "sixty") (70 . "seventy")
   (80 . "eighty") (90 . "ninety")))

(define (num->word x)
  (