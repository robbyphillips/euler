;; num->list -- changes a number to a list of its digits
;; INPUTS: Exactly one argument
;;         num -- number to be converted -- integer
;; OUTPUT: list of numbers
;;     EX: (num->list 235674) => (2 3 5 6 7 4)
(define (num->list num)
  (let ((l (string->list (string num)))
	(subs
	 '((#\0 . 0) (#\1 . 1) (#\2 . 2) (#\3 . 3) (#\4 . 4)
	   (#\5 . 5) (#\6 . 6) (#\7 . 7) (#\8 . 8) (#\9 . 9))))
    (mass-replace subs l)))

;; replace -- replaces all instances of a with b in a list l
;; INPUTS:  Exactly three arguments
;;          a -- thing to be replaced
;;          b -- thing to replace a
;;          l -- list
;; OUTPUT:  list
;;     EX:  (replace 1 2 '(0 1 0 1 0 1)) => (0 2 0 2 0 2)
(define (replace a b l)
  (cond ((null? l) l)
	((eq? a (car l))
	 (cons b (replace a b (cdr l))))
	(else (cons (car l) (replace a b (cdr l))))))

;; mass-replace -- uses replace procedure to make multiple
;;                 substitutions according to a list of pairs
;;                 (a . b) where a is replaced with b
;; INPUTS:  Exactly two arguments
;;          subs -- list of pairs of substitutions
;;          l    -- list
;; OUTPUT:  list
;;     EX:  (mass-replace '((a . b) (1 . 2)) '(a 1 a 1))
;;           => (b 2 b 2) 
(define (mass-replace subs l)
  (define (helper l i)
    (if (>= i (length subs)) l
	(helper (replace (car (list-ref subs i))
			 (cdr (list-ref subs i))
			 l)
		(+ i 1))))
  (helper l 0))

;; fact -- computes the factorial of a number
;; INPUTS:  Exactly one argument
;;          n -- an integer
;; OUTPUT:  integer
;;     EX:  (fact 3) => 6
(define (fact n)
  (if (= n 0) 1
      (* n (fact (- n 1)))))