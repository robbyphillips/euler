; Project Euler Problem 6
; Find the difference of the sum of the first 100 natural nums squared
; and the sum of the squares of the the first 100 natural nums

(define (sum-n n #!optional i)
  (cond ((eq? i #!default) (sum-n n 1))
	((> i n) 0)
	(else (+ i (sum-n n (+ i 1))))))

(define (sum-n-squares n #!optional i)
  (cond ((eq? i #!default) (sum-n-squares n 1))
	((> i n) 0)
	(else (+ (square i) (sum-n-squares n (+ i 1))))))

(- (square (sum-n 100)) (sum-n-squares 100)) ; 25164150