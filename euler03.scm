; Euler Problem 3
; Find the largest prime factor of 600851475143

(define num 600851475143)
(define *bound* (+ 1 (round (sqrt num))))

(define (lpf number #!optional i)
  (cond ((equal? i #!default) (lpf number 1))
	((<= i 0) (error "i <= 0"))	
	((>= i *bound*) 0)
	((= 0 (modulo number i)) (max i (lpf (/ number i) 2)))
	(else (lpf number (+ i 1)))))