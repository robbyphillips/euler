;;;; Project Euler Problem 8
;;;  Find the largest product of 5 consecutive digits
;;;  of the 1000-digit number.

(define the-number
7316717653133062491922511967442657474235534919493496983520312774506326239578318016984801869478851843858615607891129494954595017379583319528532088055111254069874715852386305071569329096329522744304355766896648950445244523161731856403098711121722383113622298934233803081353362766142828064444866452387493035890729629049156044077239071381051585930796086670172427121883998797908792274921901699720888093776657273330010533678812202354218097512545405947522435258490771167055601360483958644670632441572215539753697817977846174064955149290862569321978468622482839722413756570560574902614079729686524145351004748216637048440319989000889524345065854122758866688116427171479924442928230863465674813919123162824586178664583591245665294765456828489128831426076900422421902267105562632111110937054421750694165896040807198403850962455444362981230987879927244284909188845801561660979191338754992005240636899125607176060588611646710940507754100225698315520005593572972571636269561882670428252483600823257530420752963450)

;; num->list -- changes a number to a list of its digits
;; INPUTS: Exactly one argument
;;         num -- number to be converted -- integer
;; OUTPUT: list of numbers
;;     EX: (num->list 235674) => (2 3 5 6 7 4)
(define (num->list num)
  (define l (string->list (string num)))
  (define subs
    '((#\0 . 0) (#\1 . 1) (#\2 . 2) (#\3 . 3) (#\4 . 4)
      (#\5 . 5) (#\6 . 6) (#\7 . 7) (#\8 . 8) (#\9 . 9)))
  (mass-replace subs l))

;; replace -- replaces all instances of a with b in a list l
;; INPUTS:  Exactly three arguments
;;          a -- thing to be replaced
;;          b -- thing to replace a
;;          l -- list
;; OUTPUT:  list
;;     EX:  (replace 1 2 '(0 1 0 1 0 1)) => (0 2 0 2 0 2)
(define (replace a b l)
  (cond ((null? l) l)
	((eq? a (car l))
	 (cons b (replace a b (cdr l))))
	(else (cons (car l) (replace a b (cdr l))))))

;; mass-replace -- uses replace procedure to make multiple
;;                 substitutions according to a list of pairs
;;                 (a . b) where a is replaced with b
;; INPUTS:  Exactly two arguments
;;          subs -- list of pairs of substitutions
;;          l    -- list
;; OUTPUT:  list 
(define (mass-replace subs l)
  (define (helper l i)
    (if (>= i (length subs)) l
	(helper (replace (car (list-ref subs i))
			 (cdr (list-ref subs i))
			 l)
		(+ i 1))))
  (helper l 0))

;; euler -- finds the largest product possible of 5 consecutive
;;          digits within an integer
;; INPUTS:  Exactly one argument
;;          n -- integer
;; OUTPUT:  integer
(define (euler n)
  (define l (num->list n))
  (define (helper l w val)
    (cond ((< (length w) 5)
	   (helper (cdr l) (append w (list (car l))) val))
	  ((null? l) val)
	  ((> (apply * w) val)
	   (helper (cdr l) (append (cdr w) (list (car l))) (apply * w)))
	  (else
	   (helper (cdr l) (append (cdr w) (list (car l))) val))))
  (helper l '() 0))

  