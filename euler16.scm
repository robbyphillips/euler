;;;; Project Euler Problem 16
;;;  find the sum of the digits of 2^1000

(load "~/euler/helpers.scm")

(apply + (num->list (expt 2 1000))) ; => 1366