; Euler Problem 2
; Sum even Fibonacci values under four million

(define (even-fib n1 n2)
  (cond ((> (+ n1 n2) 4000000) 0)
	((= (+ n1 n2) 0)
	 (even-fib n1 (+ n2 1)))
	((even? (+ n1 n2)) 
	 (+ (+ n1 n2) (even-fib n2 (+ n1 n2))))
	(else (+ 0 (even-fib n2 (+ n1 n2))))))

(even-fib 0 0)