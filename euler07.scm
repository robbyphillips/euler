;;;; Project Euler Problem 7
;;;  Find the 10,001st prime number

(define (expmod base exp m)
  (cond ((= exp 0) 1)
	((even? exp)
	 (remainder (square (expmod base (/ exp 2) m)) m))
	(else
	 (remainder (* base (expmod base (- exp 1) m)) m))))

(define (fermat-test n)
  (define (try-it a)
    (= (expmod a n n) a))
  (try-it (+ 1 (random (- n 1)))))

(define (fast-prime? n times)
  (cond ((= times 0) #t)
	((fermat-test n) (fast-prime? n (- times 1)))
	(else #f)))

(define (smallest-divisor n)
  (define (helper n i)
    (cond ((< n (square i)) n)
	  ((= (remainder n i) 0) i)
	  (else (helper n (+ i 1)))))
  (helper n 2))

(define (prime? n)
  (= (smallest-divisor n) n))

(define (euler primes)
  ;;      x -- working value
  ;; primes -- number of primes left to find
  ;;    val -- largest prime so far
  (define (helper x primes val)
    (cond ((= primes 0) val)
	  ((prime? x)
	   (helper (+ x 1) (- primes 1) x))
	  (else
	   (helper (+ x 1) primes val))))
  (helper 2 primes 0))