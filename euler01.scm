; Project Euler #1
; Sum of all natural numbers below 1000 that are multiples of 3 or 5

(define (multiple-of-3-or-5? x)
  (if (or (integer? (/ x 3))
	  (integer? (/ x 5))) #t #f))

(define (euler x)
  (cond ((= x 0) 0)
	((multiple-of-3-or-5? x)
	 (+ x (euler (- x 1))))
	(else (+ 0 (euler (- x 1))))))

(euler 999)