;;;; Project Euler Problem 10
;;;  Calculate the sum of all primes below 2 million

(define (prime? n)
  (if (and (> n 3)
	   (or (= 0 (remainder n 2))
	       (= 0 (remainder n 3))))
      #f
      (= (smallest-divisor n) n)))

(define (smallest-divisor n)
  (define (helper n i)
    (cond ((< n (square i)) n)
	  ((= (remainder n i) 0) i)
	  (else
	   (helper n (+ i 1)))))
  (helper n 2))

(define (sum-primes bound)
  (define (helper i bound primes)
    (cond ((> i bound) primes)
	  ((prime? i)
	   (display i)
	   (newline)
	   (helper (+ i 1) bound (+ i primes)))
	  (else
	   (helper (+ i 1) bound primes))))
  (helper 2 bound 0))