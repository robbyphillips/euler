;;;; Project Euler Problem 20
;;;  Find the sum of the digits of 100!

(load "~/euler/helpers.scm")

(define (fact n)
  (if (= n 0) 1
      (* n (fact (- n 1)))))

(apply + (num->list (fact 100)))